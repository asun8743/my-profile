# Portfolio Project
Portfolio website built in HTML CSS JS


This was a website built to showcase my technical skills, web design skills.



## Project HTML Website timeline and technologies used

Build a  website in 24 hours to showcase the following:
* Introducting my self
* My Journey education
* My Job Experience 
* My technical skill set
* My other skill set
* My projects

### Technologies Used

* HTML5
* CSS3
* JavaScript (ES6)
* Git
* GitLab

## Homepage visuals

![Imgur](https://ibb.co/x3XZdYx)